package ru.t1.stroilov.tm.api.repository;

import ru.t1.stroilov.tm.model.Command;

public interface ICommandRepository {

    Command[] getCommandsArray();

}