package ru.t1.stroilov.tm.component;

import ru.t1.stroilov.tm.api.component.IBootstrap;
import ru.t1.stroilov.tm.api.controller.ICommandController;
import ru.t1.stroilov.tm.api.controller.IProjectController;
import ru.t1.stroilov.tm.api.controller.IProjectTaskController;
import ru.t1.stroilov.tm.api.controller.ITaskController;
import ru.t1.stroilov.tm.api.repository.ICommandRepository;
import ru.t1.stroilov.tm.api.repository.IProjectRepository;
import ru.t1.stroilov.tm.api.repository.ITaskRepository;
import ru.t1.stroilov.tm.api.service.ICommandService;
import ru.t1.stroilov.tm.api.service.IProjectService;
import ru.t1.stroilov.tm.api.service.IProjectTaskService;
import ru.t1.stroilov.tm.api.service.ITaskService;
import ru.t1.stroilov.tm.constant.AppConstant;
import ru.t1.stroilov.tm.constant.ArgumentConstant;
import ru.t1.stroilov.tm.controller.CommandController;
import ru.t1.stroilov.tm.controller.ProjectController;
import ru.t1.stroilov.tm.controller.ProjectTaskController;
import ru.t1.stroilov.tm.controller.TaskController;
import ru.t1.stroilov.tm.enumerated.Status;
import ru.t1.stroilov.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.stroilov.tm.exception.system.CommandNotSupportedException;
import ru.t1.stroilov.tm.model.Project;
import ru.t1.stroilov.tm.model.Task;
import ru.t1.stroilov.tm.repository.CommandRepository;
import ru.t1.stroilov.tm.repository.ProjectRepository;
import ru.t1.stroilov.tm.repository.TaskRepository;
import ru.t1.stroilov.tm.service.CommandService;
import ru.t1.stroilov.tm.service.ProjectService;
import ru.t1.stroilov.tm.service.ProjectTaskService;
import ru.t1.stroilov.tm.service.TaskService;
import ru.t1.stroilov.tm.util.TerminalUtil;

public class Bootstrap implements IBootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final IProjectTaskController projectTaskController = new ProjectTaskController(projectTaskService);

    private final IProjectController projectController = new ProjectController(projectService, projectTaskService);

    @Override
    public void run(String... args) {
        initDemoData();
        commandController.showWelcome();
        try {
            runApplication(args);
        } catch (final Exception e) {
            System.out.println(e.getMessage());
            System.out.println("[FAIL]");
        }
        runInput();
    }

    private void initDemoData() {
        projectService.add(new Project("Project1", Status.COMPLETED));
        projectService.add(new Project("Project2", Status.IN_PROGRESS));
        projectService.add(new Project("Project3", Status.IN_PROGRESS));
        projectService.add(new Project("Project4", Status.NOT_STARTED));
        taskService.add(new Task("Task1", "Task1"));
        taskService.add(new Task("Task2", "Task2"));
        taskService.add(new Task("Task3", "Task3"));
        taskService.add(new Task("Task4", "Task4"));
    }

    private void runInput() {
        System.out.println("Please enter command: ");
        while (!Thread.currentThread().isInterrupted()) {
            try {
                parseInputArgument(TerminalUtil.nextLine());
                System.out.println();
            } catch (final Exception e) {
                System.out.println(e.getMessage());
                System.out.println("[FAIL]");
            }
        }
    }

    private void runApplication(final String[] args) {
        if (args == null || args.length == 0) return;
        final String arg = args[0];
        switch (arg) {
            case ArgumentConstant.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConstant.INFO:
                commandController.showDeveloperInfo();
                break;
            case ArgumentConstant.HELP:
                commandController.showHelp();
                break;
            case ArgumentConstant.COMMANDS:
                commandController.showCommands();
                break;
            case ArgumentConstant.ARGUMENTS:
                commandController.showArguments();
                break;
            default:
                throw new ArgumentNotSupportedException(arg);
        }
        shutDownApplication();
    }

    private void parseInputArgument(final String input) {
        if (input == null || input.isEmpty()) return;
        switch (input) {
            case AppConstant.VERSION:
                commandController.showVersion();
                break;
            case AppConstant.INFO:
                commandController.showDeveloperInfo();
                break;
            case AppConstant.HELP:
                commandController.showHelp();
                break;
            case AppConstant.COMMANDS:
                commandController.showCommands();
                break;
            case AppConstant.ARGUMENTS:
                commandController.showArguments();
                break;
            case AppConstant.PROJECT_LIST:
                projectController.showProjects();
                break;
            case AppConstant.PROJECT_CREATE:
                projectController.createProject();
                break;
            case AppConstant.PROJECT_DELETE:
                projectController.clearProjects();
                break;
            case AppConstant.PROJECT_SHOW_BY_ID:
                projectController.showProjectByID();
                break;
            case AppConstant.PROJECT_SHOW_BY_INDEX:
                projectController.showProjectByIndex();
                break;
            case AppConstant.PROJECT_UPDATE_BY_ID:
                projectController.updateProjectByID();
                break;
            case AppConstant.PROJECT_UPDATE_BY_INDEX:
                projectController.updateProjectByIndex();
                break;
            case AppConstant.PROJECT_DELETE_BY_ID:
                projectController.removeProjectByID();
                break;
            case AppConstant.PROJECT_DELETE_BY_INDEX:
                projectController.removeProjectByIndex();
                break;
            case AppConstant.PROJECT_START_BY_ID:
                projectController.startProjectById();
                break;
            case AppConstant.PROJECT_START_BY_INDEX:
                projectController.startProjectByIndex();
                break;
            case AppConstant.PROJECT_COMPLETE_BY_ID:
                projectController.completeProjectById();
                break;
            case AppConstant.PROJECT_COMPLETE_BY_INDEX:
                projectController.completeProjectByIndex();
                break;
            case AppConstant.PROJECT_CHANGE_STATUS_BY_ID:
                projectController.changeProjectStatusById();
                break;
            case AppConstant.PROJECT_CHANGE_STATUS_BY_INDEX:
                projectController.changeProjectStatusByIndex();
                break;
            case AppConstant.TASK_LIST:
                taskController.showTasks();
                break;
            case AppConstant.TASK_CREATE:
                taskController.createTask();
                break;
            case AppConstant.TASK_DELETE:
                taskController.clearTasks();
                break;
            case AppConstant.TASK_SHOW_BY_ID:
                taskController.showTaskByID();
                break;
            case AppConstant.TASK_SHOW_BY_INDEX:
                taskController.showTaskByIndex();
                break;
            case AppConstant.TASK_SHOW_BY_PROJECT_ID:
                taskController.showTaskByProjectID();
                break;
            case AppConstant.TASK_UPDATE_BY_ID:
                taskController.updateTaskByID();
                break;
            case AppConstant.TASK_UPDATE_BY_INDEX:
                taskController.updateTaskByIndex();
                break;
            case AppConstant.TASK_DELETE_BY_ID:
                taskController.removeTaskByID();
                break;
            case AppConstant.TASK_DELETE_BY_INDEX:
                taskController.removeTaskByIndex();
                break;
            case AppConstant.TASK_BIND_TO_PROJECT:
                projectTaskController.bindTaskToProject();
                break;
            case AppConstant.TASK_UNBIND_FROM_PROJECT:
                projectTaskController.unbindTaskFromProject();
                break;
            case AppConstant.TASK_START_BY_ID:
                taskController.startTaskById();
                break;
            case AppConstant.TASK_START_BY_INDEX:
                taskController.startTaskByIndex();
                break;
            case AppConstant.TASK_COMPLETE_BY_ID:
                taskController.completeTaskById();
                break;
            case AppConstant.TASK_COMPLETE_BY_INDEX:
                taskController.completeTaskByIndex();
                break;
            case AppConstant.TASK_CHANGE_STATUS_BY_ID:
                taskController.changeTaskStatusById();
                break;
            case AppConstant.TASK_CHANGE_STATUS_BY_INDEX:
                taskController.changeTaskStatusByIndex();
                break;
            case AppConstant.EXIT:
                shutDownApplication();
                break;
            default:
                throw new CommandNotSupportedException(input);
        }
    }

    private void shutDownApplication() {
        System.exit(0);
    }

}