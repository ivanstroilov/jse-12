package ru.t1.stroilov.tm.service;

import ru.t1.stroilov.tm.api.repository.ICommandRepository;
import ru.t1.stroilov.tm.api.service.ICommandService;
import ru.t1.stroilov.tm.model.Command;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Command[] getCommandsArray() {
        return commandRepository.getCommandsArray();
    }

}
