package ru.t1.stroilov.tm.api.model;

public interface IHasName {

    String getName();

    void setName(String name);

}
